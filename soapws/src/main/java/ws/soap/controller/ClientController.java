package ws.soap.controller;

import javax.swing.*;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;

import ws.soap.model.User;
import ws.soap.service.UserService;
import ws.soap.view.ClientView;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;

public class ClientController {
	
	private ClientView cview;


	public ClientController() {
		cview = new ClientView();
		cview.setVisible(true);


		cview.addBtnRegActionListener(new GetActionListener());
		
		cview.addBtnLogActionListener(new Get1ActionListener());
		
		
		cview.addBtnSrcActionListener(new Get3ActionListener());

		cview.addBtnStatActionListener(new Get4ActionListener());
		
	}

	class GetActionListener implements ActionListener {
		
		public void actionPerformed(ActionEvent e) {
			
			URL wsdlURL=null;
			try {
				wsdlURL = new URL("http://localhost:8888/ws/person?wsdl");
			} catch (MalformedURLException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			//check above URL in browser, you should see WSDL file
			
			//creating QName using targetNamespace and name
			QName qname = new QName("http://service.soap.ws/", "UserServiceImplService"); 
			
			Service service = Service.create(wsdlURL, qname);  
			
			//We need to pass interface and model beans to client
			UserService ps = service.getPort(UserService.class);
			int uid=Integer.parseInt(cview.getuID());
			String uname=cview.getuName();
			String upass=cview.getpass();
			String fname=cview.getfname();
			String lname=cview.getlname();
			String email=cview.getemail();

			User u=new User(uid,uname,upass,fname,lname,email);
			
			try {
				ps.addUser(u);
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

				 
			
		}
	}
	
	

	class Get1ActionListener implements ActionListener {
		
		public void actionPerformed(ActionEvent e) {
			
			URL wsdlURL=null;
			try {
				wsdlURL = new URL("http://localhost:8888/ws/person?wsdl");
			} catch (MalformedURLException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			//check above URL in browser, you should see WSDL file
			
			//creating QName using targetNamespace and name
			QName qname = new QName("http://service.soap.ws/", "UserServiceImplService"); 
			
			Service service = Service.create(wsdlURL, qname);  
			
			//We need to pass interface and model beans to client
			UserService ps = service.getPort(UserService.class);
			
       String uname=cview.getunametolog();
       String upass=cview.getupasstologin();
       int p=0;
   	try {
		p=ps.checkUser(uname,upass);
	} catch (SQLException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
       
   	if (p==1) {
   		cview.list.setVisible(true);
   		cview.search.setVisible(true);
   		cview.status.setVisible(true);
   		try {
			ps.listallpackage(uname);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
   	}
       
			

				 
			
		}
	}
	
	
	class Get4ActionListener implements ActionListener {
		
		public void actionPerformed(ActionEvent e) {
			
			URL wsdlURL=null;
			try {
				wsdlURL = new URL("http://localhost:8888/ws/person?wsdl");
			} catch (MalformedURLException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			//check above URL in browser, you should see WSDL file
			
			//creating QName using targetNamespace and name
			QName qname = new QName("http://service.soap.ws/", "UserServiceImplService"); 
			
			Service service = Service.create(wsdlURL, qname);  
			
			//We need to pass interface and model beans to client
			UserService ps = service.getPort(UserService.class);
			String pname=cview.getpname();


			
			try {
				ps.checkStatus(pname);
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

				 
			
		}
	}
	
	
	
class Get3ActionListener implements ActionListener {
		
		public void actionPerformed(ActionEvent e) {
			
			URL wsdlURL=null;
			try {
				wsdlURL = new URL("http://localhost:8888/ws/person?wsdl");
			} catch (MalformedURLException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			//check above URL in browser, you should see WSDL file
			
			//creating QName using targetNamespace and name
			QName qname = new QName("http://service.soap.ws/", "UserServiceImplService"); 
			
			Service service = Service.create(wsdlURL, qname);  
			
			//We need to pass interface and model beans to client
			UserService ps = service.getPort(UserService.class);
			String pname=cview.getpname();


			
			try {
				ps.searchPackage(pname);
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

				 
			
		}
	}
	
	
}