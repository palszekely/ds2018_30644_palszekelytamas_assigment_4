package ws.soap.view;

import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class ClientView extends JFrame {
	

	private JPanel contentPane;
	private JTextField userID;
	private JTextField userName;
	private JTextField password;
	private JTextField firstname;
	private JTextField lastname;
	private JTextField email;
	private JButton register;
	private JTextField unametologin;
	private JTextField upasstologin;
	private JButton login;
	
	
	
	
	public JButton list;
	public JButton search;
	public JButton status;
	
	
	private JTextField pname;
	public ClientView() {
		
		setTitle("WS Swing");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(500, 300, 650, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel insertDetails = new JLabel("Register new User");
		insertDetails.setBounds(10, 11, 120, 14);
		contentPane.add(insertDetails);

		JLabel uid = new JLabel("userID");
		uid.setBounds(10, 36, 60, 14);
		contentPane.add(uid);

		JLabel uname = new JLabel("userName:");
		uname.setBounds(10, 61, 60, 14);
		contentPane.add(uname);

		JLabel pass = new JLabel("pass:");
		pass.setBounds(10, 86, 46, 14);
		contentPane.add(pass);
		
		JLabel fname1 = new JLabel("fname:");
		fname1.setBounds(10, 110, 46, 14);
		contentPane.add(fname1);

		JLabel lname1 = new JLabel("lname:");
		lname1.setBounds(10, 135, 46, 14);
		contentPane.add(lname1);

		JLabel email1 = new JLabel("email:");
		email1.setBounds(10, 160, 46, 14);
		contentPane.add(email1);


		userID = new JTextField();
		userID.setBounds(80, 33, 86, 20);
		contentPane.add(userID);
		userID.setColumns(10);

		userName = new JTextField();
		userName.setBounds(80, 58, 86, 20);
		contentPane.add(userName);
		userName.setColumns(10);

		password = new JTextField();
		password.setBounds(80, 83, 86, 20);
		contentPane.add(password);
		password.setColumns(10);
		
		firstname = new JTextField();
		firstname.setBounds(80,108, 86, 20);
		contentPane.add(firstname);
		firstname.setColumns(10);
		
		lastname = new JTextField();
		lastname.setBounds(80, 132, 86, 20);
		contentPane.add(lastname);
		lastname.setColumns(10);
		
		email = new JTextField();
		email.setBounds(80, 157, 86, 20);
		contentPane.add(email);
		email.setColumns(10);

		
		
		register = new JButton("register");
		register.setBounds(10, 178, 89, 23);
		contentPane.add(register);
		
		
		JLabel unametolog = new JLabel("username");
		unametolog.setBounds(170, 36, 120, 14);
		contentPane.add(unametolog);

		JLabel upasstolog = new JLabel("password:");
		upasstolog.setBounds(170, 61, 120, 14);
		contentPane.add(upasstolog);
		
		unametologin = new JTextField();
		unametologin.setBounds(230, 33, 86, 20);
		contentPane.add(unametologin);
		unametologin.setColumns(10);

		upasstologin = new JTextField();
		upasstologin.setBounds(230, 58, 86, 20);
		contentPane.add(upasstologin);
		upasstologin.setColumns(10);
		
		login = new JButton("login");
		login.setBounds(170, 90, 120, 23);
		contentPane.add(login);
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		JLabel uid1 = new JLabel("Package name:");
		uid1.setBounds(250, 170, 60, 14);
		contentPane.add(uid1);
		
		pname = new JTextField();
		pname.setBounds(350, 170, 86, 20);
		contentPane.add(pname);
		pname.setColumns(10);
	

		

		
		
		list = new JButton("list");
		list.setBounds(500, 30, 89, 23);
		contentPane.add(list);
		list.setVisible(false);
		
		search = new JButton("search");
		search.setBounds(450, 170, 89, 23);
		contentPane.add(search);
		search.setVisible(false);
	
		
		status = new JButton("status");
		status.setBounds(550, 170, 89, 23);
		contentPane.add(status);
		status.setVisible(false);
		
		
		
		
		
		
		
		
		
		
		
		
		
	}
	
	
	public void addBtnRegActionListener(ActionListener e) {
		register.addActionListener(e);
	}

	public void addBtnLogActionListener(ActionListener e) {
		login.addActionListener(e);
	}
	
	public void addBtnListActionListener(ActionListener e) {
		list.addActionListener(e);
	}

	public void addBtnSrcActionListener(ActionListener e) {
		search.addActionListener(e);
	}
	
	public void addBtnStatActionListener(ActionListener e) {
		status.addActionListener(e);
	}
	
	public String getpname() {
		return pname.getText();
	}
	
	

	public String getuID() {
		return userID.getText();
	}
	
	public String getuName() {
		return userName.getText();
	}

	public String getpass() {
		return password.getText();
	}
	
	public String getfname() {
		return firstname.getText();
	}
	
	public String getlname() {
		return lastname.getText();
	}

	public String getemail() {
		return email.getText();
	}
	
	public String getunametolog() {
		return unametologin.getText();
	}
	
	public String getupasstologin() {
		return upasstologin.getText();
	}

	

	
}
