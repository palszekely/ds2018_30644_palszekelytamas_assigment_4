package ws.soap.service;

import java.sql.SQLException;
import java.util.List;

import javax.jws.WebService;

import ws.soap.dao.UserDAOImpl;
import ws.soap.model.User;



@WebService(endpointInterface = "ws.soap.service.UserService") 
public class UserServiceImpl implements UserService {

	UserDAOImpl dao = new UserDAOImpl();
	
	public void addUser(User u) throws SQLException {
		dao.addUser(u);
	}
	
	public int checkUser(String uname,String pass) throws SQLException{
		return dao.checkUser(uname, pass);
	}
	
	public void checkStatus(String pname) throws SQLException{
		 dao.checkStatus(pname);
	}
	
	public void searchPackage(String pname) throws SQLException{
		 dao.searchPackage(pname);
	}
	
	public void listallpackage(String name) throws SQLException{
		 dao.listallpackage(name);
	}
	
	
	
	}
