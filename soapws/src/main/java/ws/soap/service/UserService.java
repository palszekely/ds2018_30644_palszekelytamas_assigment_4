package ws.soap.service;

import java.sql.SQLException;
import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import ws.soap.model.User;


@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface UserService {

	
	@WebMethod
	public void addUser(User u) throws SQLException;
	
	@WebMethod
	public int checkUser(String uname,String pass) throws SQLException;
	
	@WebMethod
	public void checkStatus(String pname) throws SQLException;
	
	@WebMethod
	public void searchPackage(String pname) throws SQLException;
	
	@WebMethod
	public void listallpackage(String name) throws SQLException;
	


}