package ws.soap.dao;

import java.sql.SQLException;

import ws.soap.model.User;



public interface UserDAO {
	
	void addUser(User u) throws SQLException;
	
	int checkUser(String uname,String pass) throws SQLException;
	
	void checkStatus(String pname) throws SQLException;
	
	void searchPackage(String pname) throws SQLException;
	
	void listallpackage(String pname) throws SQLException;
	
}
