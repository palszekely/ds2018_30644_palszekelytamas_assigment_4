package ws.soap.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;

import ws.soap.connection.ConnectionFactory;
import ws.soap.model.User;



public class UserDAOImpl implements UserDAO{
	private static final String insertStatementString = "INSERT INTO `user_table` VALUES (?,?,?,?,?,?)";
	private  String insertStatementString1 = "SELECT * FROM `user_table` WHERE username=? AND password=?";
	
	
	public void addUser(User u) throws SQLException {

	Connection dbConnection = (Connection) ConnectionFactory.getConnection();

	PreparedStatement insertStatement = null;
		insertStatement = (PreparedStatement) dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
		insertStatement.setInt(1, u.getId());
		insertStatement.setString(2, u.getUsername());
		insertStatement.setString(3, u.getPassword());
		insertStatement.setString(4, u.getFname());
		insertStatement.setString(5, u.getLname());
		insertStatement.setString(6, u.getEmail());
		insertStatement.executeUpdate();

		ConnectionFactory.close(insertStatement);
		ConnectionFactory.close(dbConnection);
	}
	
	public int checkUser(String uname,String pass) throws SQLException{
		
int a=0;
		Connection dbConnection = (Connection) ConnectionFactory.getConnection();
		
		PreparedStatement insertStatement = null;
		insertStatement = (PreparedStatement) dbConnection.prepareStatement("SELECT * FROM `user_table` WHERE username=? AND userpassword=?");
		insertStatement.setString(1,uname);
		insertStatement.setString(2, pass);
	 
		ResultSet resultSet = insertStatement.executeQuery();
		
		if(resultSet.next()){
			   a=1;
			}
		
		return a;
		
	}
	
	
	public void checkStatus(String pname) throws SQLException{
		

		Connection dbConnection = (Connection) ConnectionFactory.getConnection();
		
		PreparedStatement insertStatement = null;
		insertStatement = (PreparedStatement) dbConnection.prepareStatement("SELECT * FROM `package_table` WHERE pname=?");
		insertStatement.setString(1,pname);

	 
		ResultSet resultSet = insertStatement.executeQuery();
		int ts=0;
		if(resultSet.next()){
			   ts=resultSet.getInt(7);
			}
		
		if (ts==0) System.out.println("The package is on road");
		else System.out.println("The package arrived");
		
	}
	
	
	public void searchPackage(String pname) throws SQLException{
		

		Connection dbConnection = (Connection) ConnectionFactory.getConnection();
		
		PreparedStatement insertStatement = null;
		insertStatement = (PreparedStatement) dbConnection.prepareStatement("SELECT * FROM `package_table` WHERE pname=?");
		insertStatement.setString(1,pname);

	 
		ResultSet resultSet = insertStatement.executeQuery();
		if(resultSet.next()){
			   System.out.println("Sender : "+resultSet.getString(1)+" Reciever : "+resultSet.getString(2)+" Description : "+ resultSet.getString(4));
			}
		
		
		
	}
	
	
	public void listallpackage(String pname) throws SQLException{
		

		Connection dbConnection = (Connection) ConnectionFactory.getConnection();
		
		PreparedStatement insertStatement = null;
		insertStatement = (PreparedStatement) dbConnection.prepareStatement("SELECT * FROM `package_table` WHERE reciever=?");
		insertStatement.setString(1,pname);

	 
		ResultSet resultSet = insertStatement.executeQuery();
		while(resultSet.next()){
			   System.out.println("Sender : "+resultSet.getString(1)+" Description : "+ resultSet.getString(4));
			}
		
		
		
	}
	
	
}
